#!/bin/bash

install_basalt()
{
    echo "installation..."
    mkdir /usr/share/guile/2.2/basalt
    cp -R lib/* /usr/share/guile/2.2/basalt/
    echo "done."

    exit 0
}

if [ -e "/usr/share/guile/2.2" ]
then
    if [ -e "/usr/share/guile/2.2/basalt" ]
    then
	echo "BaSaLt already exists on your system."
	echo "Do you want to reinstall it ? (y/N)"
        read ANSWER

	if [ "$ANSWER" == "y" ] || [ "$ANSWER" == "Y" ]
	then
	    rm -r /usr/share/guile/2.2/basalt
	    install_basalt
	    exit 0
	else
	    echo "Bye !"
	    exit 1
	fi
	
    else
	install_basalt
    fi
else
    echo "BaSaLt only work with guile 2.2"
    exit 1
fi


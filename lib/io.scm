(define-module (basalt io))

;;;;;;;;;;;;;
;; println ;;
;;;;;;;;;;;;;

(define-public (print . args)
  (if (null? args)
      #t
      (begin
	(display (car args))
	(apply print (cdr args)))))
      

(define-public (println . args)
  (apply print args)
  (newline))

;;;;;;;;;;;
;; color ;;
;;;;;;;;;;;

(define (color-code n)
  (string-append "\x1b[" (number->string n) "m"))

(define (color-one col arg offset)
  (cond ((equal? col 'black)(string-append (color-code (+ offset 30)) arg))
	((equal? col 'red)(string-append (color-code (+ offset 31)) arg))
	((equal? col 'green)(string-append (color-code (+ offset 32)) arg))
	((equal? col 'yellow)(string-append (color-code (+ offset 33)) arg))
	((equal? col 'blue)(string-append (color-code (+ offset 34)) arg))
	((equal? col 'magenta)(string-append (color-code (+ offset 35)) arg))
	((equal? col 'cyan)(string-append (color-code (+ offset 36)) arg))
	((equal? col 'white)(string-append (color-code (+ offset 37)) arg))
	((equal? col 'bright-black)(string-append (color-code (+ offset 90)) arg))
	((equal? col 'bright-red)(string-append (color-code (+ offset 91)) arg))
	((equal? col 'bright-green)(string-append (color-code (+ offset 92)) arg))
	((equal? col 'bright-yellow)(string-append (color-code (+ offset 93)) arg))
	((equal? col 'bright-blue)(string-append (color-code (+ offset 94)) arg))
	((equal? col 'bright-magenta)(string-append (color-code (+ offset 95)) arg))
	((equal? col 'bright-cyan)(string-append (color-code (+ offset 96)) arg))
	((equal? col 'bright-white)(string-append (color-code (+ offset 97)) arg))
	(else arg)))

(define (color-res res col . args)
  (if (null? args)
      (string-append res "\x1b[0m")
      (apply color-res (string-append res (color-one col (car args) 0))
	     col (cdr args))))
    
(define-public (color col . args)
  (apply color-res "" col args))

(define (bgcolor-res res col . args)
  (if (null? args)
      (string-append res "\x1b[0m")
      (apply bgcolor-res (string-append res (color-one col (car args) 10))
	     col (cdr args))))
    
(define-public (bgcolor col . args)
  (apply bgcolor-res "" col args))

(define-module (basalt test))

(use-modules (basalt io)
	     (basalt cast))

(define-public (mk-test-env)
  (define tests '())
  
  (lambda (param)
    (cond ((equal? param '_add-test)
	   (lambda (new-name new-test)
	     (set! tests (cons (list new-name new-test) tests))
	     #t)
	   )
	  ((equal? param '_all)
	   (map (lambda (x) (car x)) tests)
	   )
	  ((equal? param '_run)
	   (lambda ()
	     (let test ((t tests)
			(counter 0)
			(errors 0))
	       (if (null? t)
		   (begin
		     (if (zero? errors)
			 (if (not (zero? counter))
			     (println (color 'green "================ All " (number->string (- counter errors)) " tests passed ================"))
			     (println (color 'yellow "No test to run")))
			 (println (color 'yellow (number->string counter) " tests : "
					 (number->string (- counter errors)) " passed, "
					 (number->string errors) " failed"))))
		   (begin
		     (let ((name (caar t))
			   (res ((cadr (car t)))))
		       
		       (if (not (car res))
			   (begin
			     (print (color 'red "Test ")
				      (color 'yellow name)
				      (color 'red " failed : "))
			     (println "expected " (cadr res) ", got " (caddr res))
			     (test (cdr t) (+ 1 counter) (+ 1 errors)))
			   (test (cdr t) (+ 1 counter) errors))))))
	   ))
	  (else
	   (cadr (assoc param tests))))))

(define-syntax assert-equal?
  (syntax-rules ()
    (
     (_ x y)
     (lambda () (list (equal? x y) x y (string-append "(assert-equal? "
						      (any->string (quote x))
						      " "
						      (any->string (quote y))
						      ")"))))))

(export assert-equal?)

(define-syntax assert?
  (syntax-rules ()
    (
     (_ x)
     (lambda () (list (equal? x #t) #t x (string-append "(assert? " (any->string (quote x)) ")"))))))

(export assert?)

(define-public (add-named-test env name test)
  ((env '_add-test) name test))

(define-public (add-test env test)
  ((env '_add-test) (cadddr (test)) test))

(define-public (run-test env)
  ((env '_run)))

(define-syntax test-pass
  (syntax-rules ()
    ((_) (lambda () (list #t 0 0 "(test-pass)")))))

(export test-pass)

(define-syntax test-fail
  (syntax-rules ()
    ((_) (lambda () (list #f #t #f "(test-fail)")))))

(export test-fail)

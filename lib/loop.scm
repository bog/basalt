(define-module (basalt loop))

(define-syntax for
  (syntax-rules (to step)
    (
     (_ x to y (body ...) ...)
     (let _a ((_i x))
       (if (equal? _i y)
	   #t
	   (begin
	     (body ...) ...
	     (set! x (+ 1 x))
	     (_a (+ _i 1))))))
    (
     (_ x to y step z (body ...) ...)
     (let _a ((_i x))
       (if (equal? _i y)
	   #t
	   (begin
	     (body ...) ...
	     (set! x (+ z x))
	     (_a (+ _i z))))))))

(export for)

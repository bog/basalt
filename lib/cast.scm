(define-module (basalt cast))

(define-public (any->string val)
  (cond
   ((string? val) val)
   ((symbol? val) (symbol->string val))
   ((number? val) (number->string val))
   ((boolean? val) (if val "#t" "#f"))
   ((list? val)
    (string-append
     "("
    (let li ((v val)
	     (res ""))
      (if (null? v)
	  res
	  (if (equal? res "")
	      (li (cdr v) (string-append res (any->string (car v))))
	      (li (cdr v) (string-append res " " (any->string (car v)))))))
    ")"
    ))
   (else 'error)))

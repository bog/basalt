(define-module (basalt sock))

(define-public (sock family proto)
  (let ((s (socket family proto 0)))
    (setsockopt s SOL_SOCKET SO_REUSEADDR 1)
    s))

(define-public (cnct socket addrstr port)
  (connect socket AF_INET (inet-pton AF_INET addrstr) port))

(define-public (lsn socket addrstr port)
  (bind socket AF_INET (inet-pton AF_INET addrstr) port)
  (listen socket 1))

(define-public (acpt socket)
  (accept socket))
